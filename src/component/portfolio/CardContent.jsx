import { AiOutlinePlus } from "react-icons/ai";
import React, { useState } from 'react';
import { Button, Modal, ModalBody} from 'reactstrap';

import Divider from "../divider/Divider"
const CardContent = (props) => {
      const [modal, setModal] = useState(false);
      const toggle = () => setModal(!modal);
      
    return (
        <div className="col-lg-4 col-sm-12 col-md-6 my-3 px-3">
            <div className="card-container">
            <a href="#tes" title="User Profile" onClick={toggle}>
                <img src={props.image} className="img-fluid port-img" alt=""/>
                <div className="overlay">
                    <i className="icon"><AiOutlinePlus /></i>
                </div>
            </a>
            </div>
            <Modal isOpen={modal} toggle={toggle} size="xl">
                <ModalBody className="text-center p-5">
                    <div className="px-5">
                        <h1 className="b-700">{props.title}</h1>
                        <Divider />
                        <img src={props.image} className="img-fluid port-img mb-5" alt=""/>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <Button className="px-3 py-2 btn-content" onClick={toggle}>X Close Button</Button>
                    </div>
                </ModalBody>
            </Modal>
        </div>
    )
}

export default CardContent
