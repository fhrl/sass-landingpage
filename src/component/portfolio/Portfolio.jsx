import { Container } from "reactstrap"
import "./portfolio.scss"
import cardimg1 from "../../assets/img/portfolio/cabin.png"
import cardimg2 from "../../assets/img/portfolio/cake.png"
import cardimg3 from "../../assets/img/portfolio/circus.png"
import cardimg4 from "../../assets/img/portfolio/game.png"
import cardimg5 from "../../assets/img/portfolio/safe.png"
import cardimg6 from "../../assets/img/portfolio/submarine.png"
import Divider from "../divider/Divider"
import CardContent from "./CardContent"


const Portfolio = (props) => {
    return (
        <div className="text-center mt-5" id="portfolio">
            <h1 className="b-700">PORTFOLIO</h1>
            <Divider />
            <Container>
                <div className="row port-card">
                    <CardContent image={cardimg1} title="LOG CABIN" />
                    <CardContent image={cardimg2} title="TASTY CAKE"/>
                    <CardContent image={cardimg3} title="CIRCUS TENT"/>
                    <CardContent image={cardimg4} title="CONTROLLER"/>
                    <CardContent image={cardimg5} title="LOCKED SAFE"/>
                    <CardContent image={cardimg6} title="SUBMARINE"/>
                </div>
            </Container>
        </div>
    )
}

export default Portfolio
