import { FaStar } from "react-icons/fa";
import "./divider.scss"
const Divider = (props) => {
    return (
        <div>
            <div className={`divider ${props.color}`}>
                <div className="divider__line"></div>
                <div className="divider__icon">
                    <FaStar size="2rem"/>
                </div>
                <div className="divider__line"></div>
            </div>
        </div>
    )
}

export default Divider
