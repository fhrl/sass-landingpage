import './about.scss'
import { FaDownload } from "react-icons/fa";
import Divider from '../../component/divider/Divider';

const About = () => {
    return (
    <div className="text-white bg-primary mt-3 py-5 about">
        <div className="text-center">
            <h2 className="text-center text-uppercase b-700 mt-5 fz-48">ABOUT</h2>
            <Divider color="putih"/>
        </div>
        <div className="d-flex flex-md-row flex-column container ">
            <div className="mx-auto">
                <p>Freelancer is a free bootstrap theme created by Start Bootstrap. The download includes the complete source files including HTML, CSS, and JavaScript as well as optional SASS stylesheets for easy customization.</p>
            </div>
            <div className="mx-auto">
                <p>You can create your own custom avatar for the masthead, change the icon in the dividers, and add your email address to the contact form to make it fully functional!</p>
            </div>
        </div>
            <div className="text-center mt-3">
                <a href="/" className="btn btn-xl button-download"><FaDownload className="me-2"/>Free Download</a>
            </div>

    </div>
    )
}
export default About
