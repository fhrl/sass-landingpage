import { FaFacebookF, FaTwitter, FaLinkedinIn, FaDribbble } from "react-icons/fa";
import "./footer.scss"

const Footer = () => {
    return (
        <>
        <div className="d-flex flex-md-row flex-column footer text-white text-center py-5">
            <div className="col-md-4 col-12 mb-3">
                <p className="b-700 f-title">LOCATION</p>
                <p className="text">2215 John Daniel <br /> Drive Clark, MO 65243</p>
            </div>
            <div className="col-md-4 col-12 mb-3">
                <p className="b-700 f-title">AROUND THE WEB</p>
                <div className="socmed justify-content-center">
                    <div className="round-border"><FaFacebookF size="24px"/></div>
                    <div className="round-border"><FaTwitter size="20px" /></div>
                    <div className="round-border"><FaLinkedinIn size="20px"/></div>
                    <div className="round-border"><FaDribbble size="20px" /></div>
                    
                </div>
            </div>
            <div className="col-md-4 col-12">
                <p className="b-700 f-title">ABOUT FREELANCER</p>
                <p className="text">Freelance is a free to use, MIT licensed Bootstrap theme created by <span className="color-primary ">Start Bootstrap .</span></p>
            </div>
        </div>
        <div className="copyright text-center text-white py-3">
            <p>Copyright © Your Website 2021</p>
        </div>
        </>
    )
}

export default Footer
