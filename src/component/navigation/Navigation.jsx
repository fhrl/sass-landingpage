import { useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import "./navigation.scss"
const Navigation = () => {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);
    return (
        <Navbar className="nav fixed-top text-uppercase px-lg-5 py-2 b-700" light expand="md">
            <NavbarBrand className="text-white navbrand">start bootstrap</NavbarBrand>
            <NavbarToggler onClick={toggle}/>
            <Collapse isOpen={isOpen} navbar className="justify-content-end">
                <Nav navbar>
                <NavItem>
                    <NavLink href="#portofolio" className="text-white mx-3">portofolio</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="#about" className="text-white mx-3">about</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink href="#contact" className="text-white mx-3">contact</NavLink>
                </NavItem>
                </Nav>
            </Collapse>
        </Navbar>
    )
}

export default Navigation
