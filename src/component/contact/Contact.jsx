import './contact.scss'
import Divider from '../../component/divider/Divider';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';

const Contact = () => { 
    return (
        <>
        <div className="text-center text-black">
        <h2 className="text-center text-uppercase b-700 mt-5 fz-48 mb-0">Contact</h2>
        <Divider />
        </div>
        <div className="d-flex flex-row justify-content-center mb-5 contact">
            <div className="col-lg-6 col-12">
                <Form>
                    <FormGroup>
                        <Label for="name">Name</Label>
                        <Input
                        type="name"
                        name="name"
                        id="name"
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="email" className="mt-2">Email</Label>
                        <Input
                        type="email"
                        name="email"
                        id="email"
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="exampleEmail" className="mt-2">Phone Number</Label>
                        <Input
                        type="text"
                        name="phone"
                        id="phone"
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="exampleText" className="mt-2">Message</Label>
                        <Input type="textarea" name="text" id="textarea" />
                    </FormGroup>
                    <Button className="mt-3 button-send">Send</Button>
                </Form>
            </div>
        </div>
        </>
    )
}
export default Contact
