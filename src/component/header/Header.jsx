import Avataaars from "../../assets/img/avataaars.svg"
import Divider from "../divider/Divider"
const Header = () =>{
    return(
    <div className="d-flex align-items-center flex-column bg-primary text-white">
        <img className="img mb-5 mt-5" width="35%" src={Avataaars} alt="Avataaars" />
        <h1 className="text-uppercase mb-0 title font-bold b-700 fz text-center">Start Bootstrap</h1>
        <Divider color="putih"/>
        <p className="content fw-300 fz-24 text-center">Graphic Artist - Web Designer - Illustrator</p>
    </div>
    )
}
export default Header