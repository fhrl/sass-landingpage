import Navigation from "../component/navigation/Navigation"
import Header from "../component/header/Header"
import Portfolio from "../component/portfolio/Portfolio"
import About from "../component/about/About"
import Contact from "../component/contact/Contact"
import Footer from "../component/footer/Footer"
const Home = () => {
    return(
    <>
        <Navigation />
        <br />
        <Header />
        <Portfolio/> 
        <About/>
        <Contact/>
        <Footer/>
    </>
        
    )
}

export default Home